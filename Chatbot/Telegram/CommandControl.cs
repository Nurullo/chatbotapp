using Microsoft.Extensions.Logging;
using NLog.Fluent;
using System;
using Telegram.Bot;
using Telegram.Bot.Args;

namespace Chatbot.Telegram
{
    public class CommandControl
    {
        private readonly ITelegramBotClient _client;
        private readonly ILogger<CommandControl> _logger;

        public CommandControl(ITelegramBotClient client, ILogger<CommandControl> logger)
        {
            _client = client;
            _logger = logger;
        }

        public void InitializeBot()
        {
            var me = _client.GetMeAsync().Result;
            _logger.LogInformation($"Hello, World! I am user {me.Id} and my name is {me.FirstName}.");
            _client.OnMessage += Bot_OnMessage;
            _client.StartReceiving();
        }

        public async void Bot_OnMessage(object sender, MessageEventArgs e)
        {
            if (e.Message.Text != null)
            {
                Console.WriteLine($"Received a text message in chat {e.Message.Chat.Id}. Message is {e.Message.Text}");

                await _client.SendTextMessageAsync(
                  chatId: e.Message.Chat,
                  text: "You said:\n" + e.Message.Text
                );
            }
        }
    }
}

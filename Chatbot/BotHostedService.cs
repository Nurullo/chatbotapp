﻿using Chatbot.Telegram;
using Microsoft.Extensions.Hosting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Chatbot
{
    public class BotHostedService : BackgroundService
    {
        private readonly CommandControl bot;

        public BotHostedService(CommandControl bot)
        {
            this.bot = bot;
        }
        protected override Task ExecuteAsync(CancellationToken stoppingToken)
        {
            bot.InitializeBot();
            return Task.CompletedTask;
        }
    }
}

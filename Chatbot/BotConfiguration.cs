﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Chatbot
{
    public class BotConfiguration
    {
        public string ApiToken { get; set; }

        public string BotUserName { get; set; }
    }
}

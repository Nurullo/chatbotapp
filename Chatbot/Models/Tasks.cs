using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Chatbot.Models
{
    public class Tasks
    {
        [Key]
        public int Id { get; set; }

        [MaxLength(300)]
        public string Task { get; set; }

        [Required]
        public DateTime CreatedAt { get; set; }

        [Required]
        public DateTime? DueTo { get; set; }
    }
}
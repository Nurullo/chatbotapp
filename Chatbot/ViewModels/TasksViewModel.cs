using System;

namespace Chatbot.ViewModels
{
    public class TasksViewModel
    {
        public int Id { get; set; }
        public string Task { get; set; }
        public DateTime? Dueto { get; set; }
    }
}
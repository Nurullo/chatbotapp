﻿using Chatbot.Models;
using Chatbot.ViewModels;
using Dapper;
using Npgsql;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace Chatbot.Repository
{
    public class TaskRepository
    {
        public IDbConnection _connection;

        public TaskRepository(IDbConnection dbConnection)
        {
            _connection = dbConnection;
        }

        /// <summary>
        /// Get All Tasks
        /// </summary>
        /// <returns></returns>
        public IEnumerable<Tasks> GetAll()
        {
            using (_connection)
            {
                _connection.Open();
                const string sql = @"SELECT * from Tasks";
                return _connection.Query<Tasks>(sql);
            }

        }

        /// <summary>
        /// Get last Task
        /// </summary>
        /// <returns></returns>
        public Tasks GetLastTask()
        {
            using (_connection)
            {
                _connection.Open();
                return _connection.Query<Tasks>("SELECT * FROM Tasks ORDER BY ID DESC LIMIT 1").FirstOrDefault();
            }
        }

        /// <summary>
        /// Create Task by tutor
        /// </summary>
        /// <param name="tasks"></param>
        /// <returns></returns>
        public int CreateTask(Tasks tasks)
        {
            using (_connection)
            {
                _connection.Open(); 
                var q = @"INSERT INTO tasks(task,createdat,dueto) VALUES
                 (@Task, @CreatedAt,@DueTo) returning id";
                return _connection.Query<int>(q, new { @Task = tasks.Task, @CreatedAt = tasks.CreatedAt, @DueTo = tasks.DueTo }).SingleOrDefault();
            }
        }

    }
}

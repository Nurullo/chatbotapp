﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Chatbot.Models;
using Chatbot.Repository;
using Chatbot.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Telegram.Bot.Types;

namespace Chatbot.Controllers
{
    [Route("api/[controller]")]
    public class TaskController : Controller
    {
        private readonly TaskRepository _task;


        public TaskController(TaskRepository task)
        {
            _task = task;
        }

        [HttpGet("AllTasks")]
        public List<Tasks> GetTasks()
        {
            return _task.GetAll().ToList();
        }
        [HttpGet("LastTask")]
        public Tasks GetLastTasks()
        {
            return _task.GetLastTask();
        }

        [HttpPost]
        public IActionResult CreateTask([FromBody]TasksViewModel model)
        {
            if (!ModelState.IsValid) return BadRequest();

            Tasks task = new Tasks()
            {
                CreatedAt = DateTime.Now,
                DueTo = model.Dueto,
                Task = model.Task,
            };

            int inserted = _task.CreateTask(task);
            if (inserted != 0)
            {
                model.Id = inserted;
                return Ok(model);
            }
            else return BadRequest();

        }
    }
}